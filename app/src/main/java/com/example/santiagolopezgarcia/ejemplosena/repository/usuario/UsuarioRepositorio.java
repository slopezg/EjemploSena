package com.example.santiagolopezgarcia.ejemplosena.repository.usuario;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.example.santiagolopezgarcia.ejemplosena.repository.RepositorioBase;
import com.example.santiagolopezgarcia.ejemplosena.model.Usuario;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by santiagolopezgarcia on 13/09/16.
 */
public class UsuarioRepositorio extends RepositorioBase implements IUsuarioRepositorio {

    static final String NombreTabla = "tbl_usuario";


    public UsuarioRepositorio(Context context) {
        super(context);
        this.operadorDatos.setNombreTabla(NombreTabla);
    }

    @Override
    public boolean guardar(Usuario dato) {
        ContentValues contentValues = convertirObjetoAContentValues(dato);
        boolean resultado = this.operadorDatos.insertar(contentValues);
        return resultado;
    }

    @Override
    public boolean actualizar(Usuario dato) {
        ContentValues datos = convertirObjetoAContentValues(dato);
        return this.operadorDatos.actualizar(datos, ColumnaUsuario.IDENTIFICACION + " = ? "
                , new String[]{dato.getIdentificacion()});
    }

    @Override
    public boolean eliminar(Usuario dato) {
        return this.operadorDatos.borrar(ColumnaUsuario.IDENTIFICACION + " = ? "
                , new String[]{dato.getIdentificacion()});
    }

    @Override
    public List<Usuario> cargarUsuarios(String apellidos) {
        List<Usuario> lista = new ArrayList<>();
        Cursor cursor = this.operadorDatos.cargar("SELECT * FROM " + NombreTabla +
                " WHERE " + ColumnaUsuario.APELLIDOS
                + " = '" + apellidos + "' ORDER BY " + ColumnaUsuario.IDENTIFICACION);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                lista.add(convertirCursorAObjeto(cursor));
                cursor.moveToNext();
            }
        }
        cursor.close();
        return lista;
    }

    @Override
    public Usuario cargarUsuario(String identificacion) {
        Usuario usuario = new Usuario();
        Cursor cursor = this.operadorDatos.cargar("SELECT * FROM " + NombreTabla +
                " WHERE " + ColumnaUsuario.IDENTIFICACION
                + " = '" + identificacion + "'");
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                usuario = convertirCursorAObjeto(cursor);
                cursor.moveToNext();
            }
        }
        cursor.close();
        return usuario;
    }

    private ContentValues convertirObjetoAContentValues(Usuario usuario) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ColumnaUsuario.IDENTIFICACION, usuario.getIdentificacion());
        contentValues.put(ColumnaUsuario.NOMBRE, usuario.getNombre());
        contentValues.put(ColumnaUsuario.APELLIDOS, usuario.getApellidos());
        return contentValues;
    }

    private Usuario convertirCursorAObjeto(Cursor cursor) {
        Usuario usuario = new Usuario();
        usuario.setIdentificacion(cursor.getString(cursor.getColumnIndex(ColumnaUsuario.IDENTIFICACION)));
        usuario.setNombre(cursor.getString(cursor.getColumnIndex(ColumnaUsuario.NOMBRE)));
        usuario.setApellidos(cursor.getString(cursor.getColumnIndex(ColumnaUsuario.APELLIDOS)));
        return usuario;
    }

    public static final String CREAR_TABLA_USUARIO = "CREATE TABLE " + UsuarioRepositorio.NombreTabla + "(" +
            ColumnaUsuario.IDENTIFICACION + " " + STRING_TYPE + " PRIMARY KEY not null," +
            ColumnaUsuario.NOMBRE + " " + STRING_TYPE + " not null," +
            ColumnaUsuario.APELLIDOS + " " + STRING_TYPE + " not null)";


    public static class ColumnaUsuario {

        public static final String IDENTIFICACION = "identificacion";
        public static final String NOMBRE = "nombre";
        public static final String APELLIDOS = "apellidos";
    }

    public static final String BORRAR_SCRIPT = "DROP TABLE IF EXISTS " + NombreTabla;

}