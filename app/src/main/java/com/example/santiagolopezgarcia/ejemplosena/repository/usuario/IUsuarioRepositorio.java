package com.example.santiagolopezgarcia.ejemplosena.repository.usuario;

import com.example.santiagolopezgarcia.ejemplosena.model.Usuario;
import com.example.santiagolopezgarcia.ejemplosena.repository.IRepositorioBase;

import java.util.List;

/**
 * Created by santiagolopezgarcia on 13/09/16.
 */
public interface IUsuarioRepositorio extends IRepositorioBase<Usuario> {
    List<Usuario> cargarUsuarios(String apellidos);

    Usuario cargarUsuario(String identificacion);
}
