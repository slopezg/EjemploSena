package com.example.santiagolopezgarcia.ejemplosena;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.santiagolopezgarcia.ejemplosena.repository.DBHelper;

/**
 * Created by santiagolopezgarcia on 13/09/16.
 */
public class OperadorDatos {

    String nombreBaseDatos;
    Context context;
    DBHelper dbHelper;
    String nombreTabla;

    public OperadorDatos(Context context, String nombreBaseDatos, int version,
                         AdministradorBaseDatosInterface administradorBaseDatosInterface) {
        dbHelper = dbHelper.getInstance(context, nombreBaseDatos, version, administradorBaseDatosInterface);
        this.context = context;
        this.nombreBaseDatos = nombreBaseDatos;
    }

    public void setNombreTabla(String nombreTabla) {
        this.nombreTabla = nombreTabla;
    }

    public boolean insertar(ContentValues contentValues) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.insert(nombreTabla, null, contentValues) > 0;
    }

    public Cursor cargar(String query) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor res = db.rawQuery(query, null);
        return res;
    }

    public boolean actualizar(ContentValues contentValues, String clause) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.update(nombreTabla, contentValues, clause, null) > 0;
    }

    public boolean actualizar(ContentValues contentValues, String clause, String[] where) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.update(nombreTabla, contentValues, clause, where) > 0;
    }

    public boolean borrar(String clause, String[] where) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete(nombreTabla, clause, where) > 0;
    }

    public void ejecutarQuery(String query) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.execSQL(query);
    }

    public void borrarTabla() {
        ejecutarQuery("Delete from " + nombreTabla);
    }
    
}
