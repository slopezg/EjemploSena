package com.example.santiagolopezgarcia.ejemplosena;

import com.example.santiagolopezgarcia.ejemplosena.repository.usuario.UsuarioRepositorio;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by santiagolopezgarcia on 13/09/16.
 */
public class AdministradorBaseDatos implements AdministradorBaseDatosInterface {

    private boolean ejecutoActualizacion;

    @Override
    public List<String> traerQuerysACrear() {
        List<String> lista = new ArrayList<>();
        if (!ejecutoActualizacion) {
            lista.add(UsuarioRepositorio.CREAR_TABLA_USUARIO);
        }
        return lista;
    }

    @Override
    public List<String> traerQuerysAModificar() {
        ejecutoActualizacion = true;
        List<String> lista = new ArrayList<>();
        return lista;
    }
}
