package com.example.santiagolopezgarcia.ejemplosena.repository;

import java.text.ParseException;
import java.util.List;

/**
 * Created by santiagolopezgarcia on 13/09/16.
 */
public interface IRepositorioBase<T> {

    boolean guardar(T dato);

    boolean actualizar(T dato);

    boolean eliminar(T dato);

}
