package com.example.santiagolopezgarcia.ejemplosena.controller;

import android.content.Context;

import com.example.santiagolopezgarcia.ejemplosena.model.Usuario;
import com.example.santiagolopezgarcia.ejemplosena.repository.usuario.UsuarioRepositorio;

/**
 * Created by santiagolopezgarcia on 13/09/16.
 */
public class UsuarioControlador {

    private UsuarioRepositorio usuarioRepositorio;

    public UsuarioControlador(Context context) {
        usuarioRepositorio = new UsuarioRepositorio(context);
    }

    public boolean guardarUsuario(Usuario usuario) {
        return usuarioRepositorio.guardar(usuario);
    }
}
