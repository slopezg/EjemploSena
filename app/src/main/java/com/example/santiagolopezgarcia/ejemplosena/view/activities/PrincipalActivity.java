package com.example.santiagolopezgarcia.ejemplosena.view.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.santiagolopezgarcia.ejemplosena.R;
import com.example.santiagolopezgarcia.ejemplosena.controller.UsuarioControlador;
import com.example.santiagolopezgarcia.ejemplosena.model.Usuario;

public class PrincipalActivity extends AppCompatActivity {

    private EditText etIdentificacion, etNombre, etApellidos;
    private Button btnGuardar;
    private UsuarioControlador usuarioControlador;
    private Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        iniciar();
        obtenerControles();
        asignarEventos();
    }

    private void iniciar() {
        usuarioControlador = new UsuarioControlador(this);
        usuario = new Usuario();
    }

    private void obtenerControles() {
        etIdentificacion = (EditText) findViewById(R.id.etIdentificacion);
        etNombre = (EditText) findViewById(R.id.etNombre);
        etApellidos = (EditText) findViewById(R.id.etApellidos);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
    }

    private void asignarEventos() {
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                usuario.setIdentificacion(etIdentificacion.getText().toString());
                usuario.setNombre(etNombre.getText().toString());
                usuario.setApellidos(etApellidos.getText().toString());

                if(usuarioControlador.guardarUsuario(usuario)){
                    Toast.makeText(PrincipalActivity.this, "Usuario guardado", Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(PrincipalActivity.this, "No se guardó usuario", Toast.LENGTH_LONG).show();
                }
            }
        });
    }


}
