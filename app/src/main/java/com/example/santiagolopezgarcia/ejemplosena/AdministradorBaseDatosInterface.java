package com.example.santiagolopezgarcia.ejemplosena;

import java.util.List;

/**
 * Created by santiagolopezgarcia on 13/09/16.
 */
public interface AdministradorBaseDatosInterface {
    List<String> traerQuerysACrear();

    List<String> traerQuerysAModificar();
}
