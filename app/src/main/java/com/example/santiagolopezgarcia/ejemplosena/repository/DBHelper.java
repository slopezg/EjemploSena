package com.example.santiagolopezgarcia.ejemplosena.repository;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.santiagolopezgarcia.ejemplosena.AdministradorBaseDatosInterface;

import java.util.List;

/**
 * Created by santiagolopezgarcia on 13/09/16.
 */
public class DBHelper extends SQLiteOpenHelper {

    private AdministradorBaseDatosInterface administradorBaseDatosInterface;
    private static DBHelper instancia;

    public DBHelper(Context context, String rutaBD, int version,
                    AdministradorBaseDatosInterface administradorBaseDatosInterface) {
        super(context, rutaBD, null, version);
        this.administradorBaseDatosInterface = administradorBaseDatosInterface;
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        if(administradorBaseDatosInterface != null){
            List<String> querys = administradorBaseDatosInterface.traerQuerysACrear();
            for (String query : querys) {
                sqLiteDatabase.execSQL(query);
            }
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        if (administradorBaseDatosInterface != null) {
            List<String> querys = administradorBaseDatosInterface.traerQuerysAModificar();
            for (String query : querys) {
                sqLiteDatabase.execSQL(query);
            }
        }
        onCreate(sqLiteDatabase);
    }


    public static synchronized DBHelper getInstance(Context context, String nombreBaseDatos,
                                                          int version, AdministradorBaseDatosInterface administradorBaseDatosInterface) {
        if (instancia == null) {
            instancia = new DBHelper(context, nombreBaseDatos, version, administradorBaseDatosInterface);
        }
        return instancia;
    }

}
