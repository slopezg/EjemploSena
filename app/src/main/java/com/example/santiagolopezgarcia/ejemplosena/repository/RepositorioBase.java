package com.example.santiagolopezgarcia.ejemplosena.repository;

import android.content.Context;
import android.os.Environment;

import com.example.santiagolopezgarcia.ejemplosena.AdministradorBaseDatos;
import com.example.santiagolopezgarcia.ejemplosena.OperadorDatos;

import java.io.File;

/**
 * Created by santiagolopezgarcia on 13/09/16.
 */
public class RepositorioBase {
    protected Context context;
    protected OperadorDatos operadorDatos;
    public static final String STRING_TYPE = "text";
    public static final String INT_TYPE = "integer";
    public static final String REAL_TYPE = "real";
    private static final String NOMBRE_BASE_DATOS = "sena.db";
    private static final int VERSION_BASE_DATOS = 1;

    public RepositorioBase(Context context) {
        this.context = context;
        String rutaBaseDatos = Environment.getExternalStorageDirectory() + File.separator  + NOMBRE_BASE_DATOS;
        operadorDatos = new OperadorDatos(context, rutaBaseDatos, VERSION_BASE_DATOS, new AdministradorBaseDatos());
    }
}
