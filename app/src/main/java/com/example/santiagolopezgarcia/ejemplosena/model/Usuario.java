package com.example.santiagolopezgarcia.ejemplosena.model;

/**
 * Created by santiagolopezgarcia on 13/09/16.
 */
public class Usuario {

    private String identificacion;
    private String nombre;
    private String apellidos;

    public Usuario() {
        identificacion = "";
        nombre = "";
        apellidos = "";
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
}
